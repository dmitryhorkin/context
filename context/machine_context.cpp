#include <context/machine_context.hpp>

#include <context/stack_builder.hpp>

namespace context {

//////////////////////////////////////////////////////////////////////

// Switch between MachineContext-s
extern "C" void SwitchMachineContext(void* from_rsp, void* to_rsp);

//////////////////////////////////////////////////////////////////////

// View for stack-saved machine context
struct StackSavedMachineContext {
  // Layout of the StackSavedMachineContext matches the layout of the stack
  // in machine_context.S at the 'Switch stacks' comment

  // Callee-saved registers
  // Saved manually in SwitchMachineContext

  void* x28;
  void* x27;
  void* x26;
  void* x25;
  void* x24;
  void* x23;
  void* x22;
  void* x21;
  void* x20;
  void* x19;

  // Saved automatically by 'call' instruction
  void* rip;
};

// https://eli.thegreenplace.net/2011/09/06/stack-frame-layout-on-x86-64/
static void MachineContextTrampoline(void*, void*, void*, void*, void*, void*, void* , void*, void* arg9, void* arg10) {
  Trampoline t = (Trampoline)arg9;
  t(arg10);
}

static void* SetupStack(wheels::MutableMemView stack, Trampoline trampoline, void* arg) {
  // https://eli.thegreenplace.net/2011/02/04/where-the-top-of-the-stack-is-on-x86/

  StackBuilder builder(stack.Back());

  // Preallocate space for arguments
  builder.Allocate(sizeof(uintptr_t) * 3);

  // Ensure trampoline will get 16-byte aligned frame pointer (rbp)
  // 'Next' here means first 'pushq %rbp' in trampoline prologue
  builder.AlignNextPush(16);

  ArgumentsListBuilder args(/*rbp=*/builder.Top());
  args.Add((void*)trampoline);
  args.Add(arg);

  // Reserve space for stack-saved context
  builder.Allocate(sizeof(StackSavedMachineContext));

  auto* stack_saved_context = (StackSavedMachineContext*)builder.Top();
  stack_saved_context->rip = (void*)MachineContextTrampoline;

  return stack_saved_context;
}

void MachineContext::Setup(wheels::MutableMemView stack, Trampoline trampoline, void* arg) {
  rsp_ = SetupStack(stack, trampoline, arg);
}

void MachineContext::SwitchTo(MachineContext& target) {
  SwitchMachineContext(&rsp_, &target.rsp_);
}

}  // namespace context
